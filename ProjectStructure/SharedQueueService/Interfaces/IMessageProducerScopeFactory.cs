﻿using SharedQueueService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Interfaces
{
    public interface IMessageProducerScopeFactory
    {
        IMessageProducerScope Open(MessageScopeSetting messageScopeSetting);
    }
}
