﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Interfaces
{
    public interface IMessageProducerScope:IDisposable
    {
        IMessageProducer MessageProducer { get; }
    }
}
