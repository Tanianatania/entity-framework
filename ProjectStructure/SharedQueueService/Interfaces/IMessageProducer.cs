﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Interfaces
{
    public interface IMessageProducer
    {
        void Send(string message, string type = null);
    }
}
