﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Models
{
    public class MessageProducerSetting
    {
        public IModel Channel { get; set; }
        public PublicationAddress PublicationAddress { get; set; }
    }
}
