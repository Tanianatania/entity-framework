﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using System;

namespace SharedQueueService.QueueServices 
{
    public class MessageConsumer : IMessageConsumer
    {
        private readonly MessageConsumerSetting _setting;
        private readonly EventingBasicConsumer _consumer;

        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }

        public MessageConsumer(MessageConsumerSetting setting)
        {
            _setting = setting;
            _consumer = new EventingBasicConsumer(_setting.Channel);
        }

        public void Connect()
        {
            if(_setting.SequentialFetch)
            {
                _setting.Channel.BasicQos(0, 1, false);
            }
            _setting.Channel.BasicConsume(_setting.QueueName, _setting.AutoActnowledge, _consumer);
        }

        public void SetActnowledge(ulong deliveryTag,bool processe)
        {
            if (processe)
            {
                _setting.Channel.BasicAck(deliveryTag, false);
            }
            else
            {
                _setting.Channel.BasicNack(deliveryTag, false, true);
            }
        }
    }
}
