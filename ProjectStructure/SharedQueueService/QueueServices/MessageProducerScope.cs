﻿using RabbitMQ.Client;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using System;

namespace SharedQueueService.QueueServices
{
    public class MessageProducerScope: IMessageProducerScope
    {
        private readonly Lazy<IMessageQueue> _messageQueueLazy;
        private readonly Lazy<IMessageProducer> _messageProducerLazy;

        private readonly MessageScopeSetting _messageScopeSetting;
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScope(IConnectionFactory connectionFactory, MessageScopeSetting messageScopeSetting)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSetting = messageScopeSetting;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageProducerLazy = new Lazy<IMessageProducer>(CreateMessangeProducer);
        }

        public IMessageProducer MessageProducer => _messageProducerLazy.Value;
        private IMessageQueue MessageQueue => _messageQueueLazy.Value;
        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSetting);
        }

        private IMessageProducer CreateMessangeProducer()
        {
            return new MessageProducer(new MessageProducerSetting
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    _messageScopeSetting.ExchangeType,
                    _messageScopeSetting.ExchangeName,
                    _messageScopeSetting.RoutingKey)
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
