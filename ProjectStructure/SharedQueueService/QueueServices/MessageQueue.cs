﻿using RabbitMQ.Client;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.QueueServices
{
    public class MessageQueue : IMessageQueue
    {
        private readonly IConnection _connection;
        public IModel Channel { get; protected set; }

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory,MessageScopeSetting messageScopeSetting)
            :this (connectionFactory)
        {
            DeclareExchange(messageScopeSetting.ExchangeName, messageScopeSetting.ExchangeType);

            if(messageScopeSetting.QueueName!=null)
            {
                BinaQueue(messageScopeSetting.ExchangeName, messageScopeSetting.RoutingKey, messageScopeSetting.QueueName);
            }
        }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        public void BinaQueue(string exchangeName,string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
