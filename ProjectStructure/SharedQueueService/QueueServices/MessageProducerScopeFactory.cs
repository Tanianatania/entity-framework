﻿using RabbitMQ.Client;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;

namespace SharedQueueService.QueueServices
{
    public class MessageProducerScopeFactory: IMessageProducerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageProducerScope Open(MessageScopeSetting messageScopeSetting)
        {
            return new MessageProducerScope(_connectionFactory, messageScopeSetting);
        }
    }
}
