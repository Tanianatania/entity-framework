﻿using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using DAL.Entities;

namespace ProjectStructure.Profilies
{
    public class LinqProfile:Profile
    {
        public LinqProfile()
        {
            CreateMap<FinishedTasksDTO, FinishedTasksDTO>();
            CreateMap<FinishedTasksDTO, FinishedTasksDTO>();

            CreateMap<TeamWithUserDTO, TeamWithUserDTO>();
            CreateMap<TeamWithUserDTO, TeamWithUserDTO>();

            CreateMap<UserWithTasksDTO, UserWithTasksDTO>();
            CreateMap<UserWithTasksDTO, TeamWithUserDTO>();
        }
    }
}
