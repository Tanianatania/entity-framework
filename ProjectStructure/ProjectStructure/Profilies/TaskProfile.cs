﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace ProjectStructure.Models.Profilies
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Tasks, TasksDTO>();
            CreateMap<TasksDTO, Tasks>();
        }
    }
}