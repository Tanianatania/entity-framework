﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IService<TeamDTO> _service;

        public TeamsController(IService<TeamDTO> service)
        {
            _service = service;
        }

        // GET: api/Teams
        [HttpGet]
        public IEnumerable<TeamDTO> Get()
        {
            return _service.GetList();
        }

        // GET: api/Teams/5
        [HttpGet("{id}", Name = "GetTeam")]
        public TeamDTO Get(int id)
        {
            return _service.Get(id);
        }

        // POST: api/Teams
        [HttpPost]
        public void Post([FromBody] string value)
        {
            TeamDTO item = JsonConvert.DeserializeObject<TeamDTO>(value);
            _service.Create(item);
        }

        // PUT: api/Teams
        [HttpPut]
        public void Put([FromBody] string value)
        {
            TeamDTO item = JsonConvert.DeserializeObject<TeamDTO>(value);
            _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
