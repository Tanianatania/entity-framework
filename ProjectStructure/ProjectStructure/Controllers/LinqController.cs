﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly ILinqService _service;

        public LinqController(ILinqService service)
        {
            _service = service;
        }

        [Route("getCountOfTaskInProjectByUserId/{id}")]
        [HttpGet]
        public IEnumerable<CountOfTasksInProjectsDTO> GetCountOfTaskInProjectByUserId(int id)
        {
            return _service.GetCountOfTaskInProjectByUserId(id);
        }

        [Route("getByProjectId/{id}")]
        [HttpGet]
        public ProjectInformationDTO GetByProjectId(int id)
        {
            return _service.GetByProjectId(id);
        }

        [Route("getByUserId/{id}")]
        [HttpGet]
        public UserInformationDTO GetByUserId(int id)
        {
           return _service.GetByUserId(id);
        }

        [Route("getFinishesTasks/{id}")]
        [HttpGet]
        public IEnumerable<FinishedTasksDTO> GetFinishedTasks(int id)
        {
            return _service.GetFinishedTasks(id);
        }

        [Route("getSortedList")]
        [HttpGet]
        public IEnumerable<UserWithTasksDTO> GetSortedListByUserAndTeam()
        {
            return _service.GetSortedListByUserAndTeam();
        }

        [Route("getAllFromFile")]
        public IEnumerable<ResultModelDTO> GetAllFromFile()
        {
            return _service.GetAllFromFile().OrderByDescending(a=>a.Date);
        }

        [Route("getTasksOfUser/{id}")]
        [HttpGet]
        public IEnumerable<TasksDTO> GetTaskOfUser(int id)
        {
            return _service.GetTaskOfUser(id);
        }

        [Route("getTeamWithYoungUsers")]
        [HttpGet]
        public IEnumerable<TeamWithUserDTO> GetTeamWithYoungUsers()
        {
            return _service.GetTeamWithYoungUser();
        }
    }
}