﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using ProjectStructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IService<UserDTO> _service;

        public UsersController(IService<UserDTO> service)
        {
            _service = service;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<UserDTO> Get()
        {
            return _service.GetList();
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUser")]
        public UserDTO Get(int id)
        {
            return _service.Get(id);
        }

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] string value)
        {
            UserDTO item = JsonConvert.DeserializeObject<UserDTO>(value);
            _service.Create(item);
        }

        // PUT: api/Users/5
        [HttpPut]
        public void Put( [FromBody] string value)
        {
            UserDTO item = JsonConvert.DeserializeObject<UserDTO>(value);
            _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
