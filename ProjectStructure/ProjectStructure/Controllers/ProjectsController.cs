﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _service;

        public ProjectsController(IService<ProjectDTO> service)
        {
            _service = service;
        }
        // GET: api/Projects
        [HttpGet]
        public IEnumerable<ProjectDTO> Get()
        {
            return _service.GetList();
        }

        // GET: api/Projects/5
        [HttpGet("{id}", Name = "GetProject")]
        public ProjectDTO Get(int id)
        {
            return _service.Get(id);
        }

        // POST: api/Projects
        [HttpPost]
        public void Post([FromBody] string value)
        {
            ProjectDTO item = JsonConvert.DeserializeObject<ProjectDTO>(value);
            _service.Create(item);
        }

        // PUT: api/Projects/5
        [HttpPut]
        public void Put([FromBody] string value)
        {
            ProjectDTO item = JsonConvert.DeserializeObject<ProjectDTO>(value);
            _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
