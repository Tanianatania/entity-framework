﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IService<TasksDTO> _service;

        public TasksController(IService<TasksDTO> service)
        {
            _service = service;
        }
        // GET: api/Tasks
        [HttpGet]
        public IEnumerable<TasksDTO> Get()
        {
            return _service.GetList();
        }

        // GET: api/Tasks/5
        [HttpGet("{id}", Name = "GetTasks")]
        public TasksDTO Get(int id)
        {
            return _service.Get(id);
        }

        // POST: api/Tasks
        [HttpPost]
        public void Post([FromBody] string value)
        {
            TasksDTO item = JsonConvert.DeserializeObject<TasksDTO>(value);
            _service.Create(item);
        }

        // PUT: api/Tasks
        [HttpPut]
        public void Put([FromBody] string value)
        {
            TasksDTO item = JsonConvert.DeserializeObject<TasksDTO>(value);
            _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
