﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStateModelsController : ControllerBase
    {
        private readonly IService<TaskStateModelDTO> _service;
        public TaskStateModelsController(IService<TaskStateModelDTO> service)
        {
            _service = service;
        }

        // GET: api/TaskStateModels
        [HttpGet]
        public IEnumerable<TaskStateModelDTO> Get()
        {
            return _service.GetList();
        }

        // GET: api/TaskStateModels/5
        [HttpGet("{id}", Name = "GetTaskStateModel")]
        public TaskStateModelDTO Get(int id)
        {
            return _service.Get(id);
        }

        // POST: api/TaskStateModels
        [HttpPost]
        public void Post([FromBody] string value)
        {
            TaskStateModelDTO item = JsonConvert.DeserializeObject<TaskStateModelDTO>(value);
            _service.Create(item);
        }

        // PUT: api/TaskStateModels
        [HttpPut]
        public void Put([FromBody] string value)
        {
            TaskStateModelDTO item = JsonConvert.DeserializeObject<TaskStateModelDTO>(value);
            _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
