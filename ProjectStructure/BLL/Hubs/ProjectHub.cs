﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace BLL.Hubs
{
    public sealed class ProjectHub : Hub
    {
        public async Task Send(string value)
        {
            await Clients.All.SendAsync(value);
        }
    }
}