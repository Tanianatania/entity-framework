﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO.LinqModels
{
    public class UserInformationDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO MinProject { get; set; }
        public int CountOfTaskInMinProject { get; set; }
        public int CountOfCanceledOrNotFinishedTasks { get; set; }
        public TasksDTO LongestTask { get; set; }
    }
}
