﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO.LinqModels
{
    public class FinishedTasksDTO
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
