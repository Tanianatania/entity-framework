﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO.LinqModels
{
    public class ResultModelDTO
    {
        public string Message { get; set; }
        public DateTime Date { get; set; }
    }
}
