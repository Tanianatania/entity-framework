﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public virtual TeamDTO Team { get; set; }

        public override string ToString()
        {
            return $" First name: {FirstName}, Last name: {LastName}, Email: {Email}, Birthday: {Birthday}," +
                $"Registered at: {RegisteredAt}, Team id: {TeamId} ";
        }
    }
}
