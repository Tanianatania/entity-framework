﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class TasksDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        public int ProjectId { get; set; }
        public virtual ProjectDTO Project { get; set; }
        public int PerformerId { get; set; }
        public virtual UserDTO Perfomer { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Description: {Description.Replace("\n"," ")}, Create at: {CreatedAt}, Finished at: {FinishedAt}" +
                $" State: {State}, Project Id: {ProjectId}, Perfomer Id: {PerformerId}\n";
        }
    }
}
