﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TaskRepository : IRepository<Tasks>
    {

        private readonly ApplicationDbContext _context;

        public TaskRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public IEnumerable<Tasks> GetList()
        {
            return _context.Tasks
                .Include(a=>a.Perfomer)
                .Include(a=>a.Project);
        }

        public Tasks Get(int id)
        {
            return _context.Tasks.Where(a => a.Id == id)
                .Include(a => a.Perfomer)
                .Include(a => a.Project)
                .FirstOrDefault();
        }

        public void Create(Tasks item)
        {
            _context.Tasks.Add(item);
        }

        public void Update(Tasks item)
        {
            var oldTasks = _context.Tasks.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.Tasks.Remove(oldTasks);
                _context.Tasks.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _context.Tasks.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.Tasks.Remove(oldTasks);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}

