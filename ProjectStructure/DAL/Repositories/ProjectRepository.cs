﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
   public class ProjectRepository : IRepository<Project>
    {
        private readonly ApplicationDbContext _context;

        public ProjectRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public IEnumerable<Project> GetList()
        {
            return _context.Projects
                .Include(a=>a.Author)
                .Include(a=>a.Team);
        }

        public Project Get(int id)
        {
            return _context.Projects.Where(a => a.Id == id)
                .Include(a => a.Author)
                .Include(a => a.Team)
                .FirstOrDefault();
        }

        public void Create(Project item)
        {
            _context.Projects.Add(item);
        }

        public void Update(Project item)
        {
            var oldProject = _context.Projects.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldProject != null)
            {
                var old= _context.Projects.Remove(oldProject);
                _context.Projects.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldProject = _context.Projects.Where(a => a.Id == id).FirstOrDefault();
            if (oldProject != null)
            {
                _context.Projects.Remove(oldProject);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
