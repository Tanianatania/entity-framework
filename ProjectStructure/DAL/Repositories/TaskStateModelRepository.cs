﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public class TaskStateModelRepository : IRepository<TaskStateModel>
    {
        private readonly ApplicationDbContext _context;

        public TaskStateModelRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public IEnumerable<TaskStateModel> GetList()
        {
            return _context.TaskStateModels;
        }

        public TaskStateModel Get(int id)
        {
            return _context.TaskStateModels.Where(a => a.Id == id).FirstOrDefault();
        }

        public void Create(TaskStateModel item)
        {
            _context.TaskStateModels.Add(item);
        }

        public void Update(TaskStateModel item)
        {
            var oldTasks = _context.TaskStateModels.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.TaskStateModels.Remove(oldTasks);
                _context.TaskStateModels.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _context.TaskStateModels.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.TaskStateModels.Remove(oldTasks);
            }
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        #endregion
    }
}
