﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public IEnumerable<User> GetList()
        {
            return _context.Users
                .Include(a=>a.Team);
        }

        public User Get(int id)
        {
            return _context.Users.Where(a => a.Id == id)
                .Include(a => a.Team)
                .FirstOrDefault();
        }

        public void Create(User item)
        {
            _context.Users.Add(item);
        }

        public void Update(User item)
        {
            var oldTasks = _context.Users.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.Users.Remove(oldTasks);
                _context.Users.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _context.Users.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.Users.Remove(oldTasks);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
