﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly ApplicationDbContext _context;

        public TeamRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public IEnumerable<Team> GetList()
        {
            return _context.Teams;
        }

        public Team Get(int id)
        {
            return _context.Teams.Where(a => a.Id == id).FirstOrDefault();
        }

        public void Create(Team item)
        {
            _context.Teams.Add(item);
        }

        public void Update(Team item)
        {
            var oldTasks = _context.Teams.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.Teams.Remove(oldTasks);
                _context.Teams.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _context.Teams.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _context.Teams.Remove(oldTasks);
            }
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        #endregion
    }
}
