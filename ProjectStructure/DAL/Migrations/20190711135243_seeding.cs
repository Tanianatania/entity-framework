﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Author_id", "Created_at", "Deadline", "Description", "Name", "Team_id" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2019, 6, 29, 16, 52, 43, 472, DateTimeKind.Local).AddTicks(2590), new DateTime(2019, 7, 23, 16, 52, 43, 474, DateTimeKind.Local).AddTicks(7834), "It is project 1", "Project 1", 1 },
                    { 2, 1, new DateTime(2019, 7, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(318), new DateTime(2019, 11, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(341), "It is project 2", "Project 2", 1 }
                });

            migrationBuilder.InsertData(
                table: "TaskStateModels",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "creaete" },
                    { 2, "canceled" },
                    { 3, "finished" }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Created_at", "Description", "Finished_at", "Name", "Performer_id", "Project_id", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 5, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(3083), "It is task 1", new DateTime(2019, 10, 19, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(4327), "Task 1", 1, 1, 3 },
                    { 2, new DateTime(2019, 5, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(6056), "It is task 2", new DateTime(2019, 10, 19, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(6079), "Task 2", 1, 2, 1 }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Created_at", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 22, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(7981), "Team 1" },
                    { 2, new DateTime(2019, 7, 30, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(8621), "Team 2" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "First_name", "Last_name", "Registered_at", "Team_id" },
                values: new object[,]
                {
                    { 1, new DateTime(2014, 4, 28, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(1114), "user1@gmail.com", "First", "Last", new DateTime(2019, 6, 22, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(2461), 1 },
                    { 2, new DateTime(1992, 2, 23, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(3768), "user2@gmail.com", "Name", "Last", new DateTime(2019, 6, 22, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(3794), 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStateModels",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStateModels",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStateModels",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
