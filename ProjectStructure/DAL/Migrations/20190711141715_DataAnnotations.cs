﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class DataAnnotations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "First_name",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Last_name",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "Team_id",
                table: "Users",
                newName: "TeamId");

            migrationBuilder.RenameColumn(
                name: "Registered_at",
                table: "Users",
                newName: "RegisteredAt");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Users",
                maxLength: 15,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Users",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                maxLength: 15,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "TaskStateModels",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                maxLength: 15,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created_at", "Deadline" },
                values: new object[] { new DateTime(2019, 6, 29, 17, 17, 14, 512, DateTimeKind.Local).AddTicks(4448), new DateTime(2019, 7, 23, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(480) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created_at", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 11, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(3631), new DateTime(2019, 11, 11, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(3653) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created_at", "Finished_at" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(7635), new DateTime(2019, 10, 19, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(9342) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created_at", "Finished_at" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(1897), new DateTime(2019, 10, 19, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(1933) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created_at",
                value: new DateTime(2019, 6, 22, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(5706));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created_at",
                value: new DateTime(2019, 7, 30, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(6817));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 28, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(1448), "First", "Last", new DateTime(2019, 6, 22, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(3382) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 2, 23, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(5354), "Name", "Last", new DateTime(2019, 6, 22, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(5403) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "TeamId",
                table: "Users",
                newName: "Team_id");

            migrationBuilder.RenameColumn(
                name: "RegisteredAt",
                table: "Users",
                newName: "Registered_at");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "First_name",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Last_name",
                table: "Users",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 15);

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "TaskStateModels",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 15,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created_at", "Deadline" },
                values: new object[] { new DateTime(2019, 6, 29, 16, 52, 43, 472, DateTimeKind.Local).AddTicks(2590), new DateTime(2019, 7, 23, 16, 52, 43, 474, DateTimeKind.Local).AddTicks(7834) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created_at", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(318), new DateTime(2019, 11, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(341) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created_at", "Finished_at" },
                values: new object[] { new DateTime(2019, 5, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(3083), new DateTime(2019, 10, 19, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(4327) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created_at", "Finished_at" },
                values: new object[] { new DateTime(2019, 5, 11, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(6056), new DateTime(2019, 10, 19, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(6079) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created_at",
                value: new DateTime(2019, 6, 22, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(7981));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created_at",
                value: new DateTime(2019, 7, 30, 16, 52, 43, 475, DateTimeKind.Local).AddTicks(8621));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "First_name", "Last_name", "Registered_at" },
                values: new object[] { new DateTime(2014, 4, 28, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(1114), "First", "Last", new DateTime(2019, 6, 22, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(2461) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "First_name", "Last_name", "Registered_at" },
                values: new object[] { new DateTime(1992, 2, 23, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(3768), "Name", "Last", new DateTime(2019, 6, 22, 16, 52, 43, 476, DateTimeKind.Local).AddTicks(3794) });
        }
    }
}
