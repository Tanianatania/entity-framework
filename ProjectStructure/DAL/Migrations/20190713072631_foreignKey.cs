﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class foreignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 1, 10, 26, 30, 539, DateTimeKind.Local).AddTicks(5226), new DateTime(2019, 7, 25, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(62) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 13, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(2790), new DateTime(2019, 11, 13, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(2813) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 13, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(6319), new DateTime(2019, 10, 21, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(7968) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 13, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(306), new DateTime(2019, 10, 21, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(337) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 6, 24, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(3043));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 8, 1, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(3937));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 30, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(7163), new DateTime(2019, 6, 24, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(8723) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 2, 25, 10, 26, 30, 545, DateTimeKind.Local).AddTicks(484), new DateTime(2019, 6, 24, 10, 26, 30, 545, DateTimeKind.Local).AddTicks(528) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 6, 29, 17, 21, 29, 721, DateTimeKind.Local).AddTicks(3142), new DateTime(2019, 7, 23, 17, 21, 29, 738, DateTimeKind.Local).AddTicks(5947) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 11, 17, 21, 29, 738, DateTimeKind.Local).AddTicks(8631), new DateTime(2019, 11, 11, 17, 21, 29, 738, DateTimeKind.Local).AddTicks(8653) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(2205), new DateTime(2019, 10, 19, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(3841) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(6125), new DateTime(2019, 10, 19, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(6161) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 6, 22, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(8845));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 7, 30, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(9729));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 28, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(3093), new DateTime(2019, 6, 22, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(4644) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 2, 23, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(6360), new DateTime(2019, 6, 22, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(6404) });
        }
    }
}
