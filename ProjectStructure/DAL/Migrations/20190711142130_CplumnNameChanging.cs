﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class CplumnNameChanging : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Created_at",
                table: "Teams",
                newName: "CreatedAt");

            migrationBuilder.RenameColumn(
                name: "Project_id",
                table: "Tasks",
                newName: "ProjectId");

            migrationBuilder.RenameColumn(
                name: "Performer_id",
                table: "Tasks",
                newName: "PerformerId");

            migrationBuilder.RenameColumn(
                name: "Finished_at",
                table: "Tasks",
                newName: "FinishedAt");

            migrationBuilder.RenameColumn(
                name: "Created_at",
                table: "Tasks",
                newName: "CreatedAt");

            migrationBuilder.RenameColumn(
                name: "Team_id",
                table: "Projects",
                newName: "TeamId");

            migrationBuilder.RenameColumn(
                name: "Created_at",
                table: "Projects",
                newName: "CreatedAt");

            migrationBuilder.RenameColumn(
                name: "Author_id",
                table: "Projects",
                newName: "AuthorId");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 6, 29, 17, 21, 29, 721, DateTimeKind.Local).AddTicks(3142), new DateTime(2019, 7, 23, 17, 21, 29, 738, DateTimeKind.Local).AddTicks(5947) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 11, 17, 21, 29, 738, DateTimeKind.Local).AddTicks(8631), new DateTime(2019, 11, 11, 17, 21, 29, 738, DateTimeKind.Local).AddTicks(8653) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(2205), new DateTime(2019, 10, 19, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(3841) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(6125), new DateTime(2019, 10, 19, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(6161) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 6, 22, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(8845));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 7, 30, 17, 21, 29, 739, DateTimeKind.Local).AddTicks(9729));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 28, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(3093), new DateTime(2019, 6, 22, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(4644) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 2, 23, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(6360), new DateTime(2019, 6, 22, 17, 21, 29, 740, DateTimeKind.Local).AddTicks(6404) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "Teams",
                newName: "Created_at");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Tasks",
                newName: "Project_id");

            migrationBuilder.RenameColumn(
                name: "PerformerId",
                table: "Tasks",
                newName: "Performer_id");

            migrationBuilder.RenameColumn(
                name: "FinishedAt",
                table: "Tasks",
                newName: "Finished_at");

            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "Tasks",
                newName: "Created_at");

            migrationBuilder.RenameColumn(
                name: "TeamId",
                table: "Projects",
                newName: "Team_id");

            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "Projects",
                newName: "Created_at");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Projects",
                newName: "Author_id");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created_at", "Deadline" },
                values: new object[] { new DateTime(2019, 6, 29, 17, 17, 14, 512, DateTimeKind.Local).AddTicks(4448), new DateTime(2019, 7, 23, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(480) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created_at", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 11, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(3631), new DateTime(2019, 11, 11, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(3653) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created_at", "Finished_at" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(7635), new DateTime(2019, 10, 19, 17, 17, 14, 516, DateTimeKind.Local).AddTicks(9342) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created_at", "Finished_at" },
                values: new object[] { new DateTime(2019, 5, 11, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(1897), new DateTime(2019, 10, 19, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(1933) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created_at",
                value: new DateTime(2019, 6, 22, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(5706));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created_at",
                value: new DateTime(2019, 7, 30, 17, 17, 14, 517, DateTimeKind.Local).AddTicks(6817));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 28, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(1448), new DateTime(2019, 6, 22, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(3382) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 2, 23, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(5354), new DateTime(2019, 6, 22, 17, 17, 14, 518, DateTimeKind.Local).AddTicks(5403) });
        }
    }
}
