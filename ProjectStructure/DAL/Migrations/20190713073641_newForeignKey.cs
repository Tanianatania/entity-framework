﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class newForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PerfomerId",
                table: "Tasks",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 1, 10, 36, 40, 499, DateTimeKind.Local).AddTicks(950), new DateTime(2019, 7, 25, 10, 36, 40, 502, DateTimeKind.Local).AddTicks(5088) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 13, 10, 36, 40, 502, DateTimeKind.Local).AddTicks(7866), new DateTime(2019, 11, 13, 10, 36, 40, 502, DateTimeKind.Local).AddTicks(7897) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 13, 10, 36, 40, 503, DateTimeKind.Local).AddTicks(1874), new DateTime(2019, 10, 21, 10, 36, 40, 503, DateTimeKind.Local).AddTicks(3594) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 13, 10, 36, 40, 503, DateTimeKind.Local).AddTicks(5946), new DateTime(2019, 10, 21, 10, 36, 40, 503, DateTimeKind.Local).AddTicks(5981) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 6, 24, 10, 36, 40, 503, DateTimeKind.Local).AddTicks(8554));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 8, 1, 10, 36, 40, 503, DateTimeKind.Local).AddTicks(9439));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 30, 10, 36, 40, 504, DateTimeKind.Local).AddTicks(2866), new DateTime(2019, 6, 24, 10, 36, 40, 504, DateTimeKind.Local).AddTicks(4457) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 2, 25, 10, 36, 40, 504, DateTimeKind.Local).AddTicks(6234), new DateTime(2019, 6, 24, 10, 36, 40, 504, DateTimeKind.Local).AddTicks(6283) });

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerfomerId",
                table: "Tasks",
                column: "PerfomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Users_PerfomerId",
                table: "Tasks",
                column: "PerfomerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Teams_TeamId",
                table: "Users",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Users_PerfomerId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Teams_TeamId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_TeamId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_PerfomerId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_TeamId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "PerfomerId",
                table: "Tasks");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 1, 10, 26, 30, 539, DateTimeKind.Local).AddTicks(5226), new DateTime(2019, 7, 25, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(62) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 7, 13, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(2790), new DateTime(2019, 11, 13, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(2813) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 13, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(6319), new DateTime(2019, 10, 21, 10, 26, 30, 543, DateTimeKind.Local).AddTicks(7968) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2019, 5, 13, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(306), new DateTime(2019, 10, 21, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(337) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 6, 24, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(3043));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 8, 1, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(3937));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 30, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(7163), new DateTime(2019, 6, 24, 10, 26, 30, 544, DateTimeKind.Local).AddTicks(8723) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(1992, 2, 25, 10, 26, 30, 545, DateTimeKind.Local).AddTicks(484), new DateTime(2019, 6, 24, 10, 26, 30, 545, DateTimeKind.Local).AddTicks(528) });
        }
    }
}
