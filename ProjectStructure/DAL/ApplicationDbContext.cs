﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace DAL
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions options)
            :base(options)
        {
        }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<TaskStateModel> TaskStateModels { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id=1,
                    Name="Project 1",
                    Description="It is project 1",
                    CreatedAt=DateTime.Now.AddDays(-12),
                    Deadline=DateTime.Now.AddDays(12),
                    AuthorId=1,
                    TeamId=1
                },
                new Project
                {
                    Id=2,
                    Name="Project 2",
                    Description="It is project 2",
                    CreatedAt=DateTime.Now,
                    Deadline=DateTime.Now.AddDays(123),
                    AuthorId=1,
                    TeamId=1
                }
            };
            var tasks = new List<Tasks>
            {
                new Tasks
                {
                    Id=1,
                    Name="Task 1",
                    State=3,
                    CreatedAt=DateTime.Now.AddMonths(-2),
                    Description="It is task 1",
                    FinishedAt=DateTime.Now.AddDays(100),
                    PerformerId=1,
                    ProjectId=1
                },
                new Tasks
                {
                    Id=2,
                    Name="Task 2",
                    State=1,
                    CreatedAt=DateTime.Now.AddMonths(-2),
                    Description="It is task 2",
                    FinishedAt=DateTime.Now.AddDays(100),
                    PerformerId=1,
                    ProjectId=2
                }
            };
            var teams = new List<Team>
            {
                new Team
                {
                    Id=1,
                    Name="Team 1",
                    CreatedAt=DateTime.Now.AddDays(-19)
                },
                new Team
                {
                    Id=2,
                    Name="Team 2",
                    CreatedAt=DateTime.Now.AddDays(19)
                }
            };
            var users = new List<User>
            {
                new User
            {
                Id=1,
                FirstName="First",
                LastName="Last",
                Birthday=DateTime.Now.AddDays(-1900),
                Email="user1@gmail.com",
                RegisteredAt=DateTime.Now.AddDays(-19),
                TeamId=1
            },
                new User
                {
                    Id=2,
                    Birthday=DateTime.Now.AddDays(-10000),
                    Email="user2@gmail.com",
                    FirstName="Name",
                    LastName="Last",
                    RegisteredAt=DateTime.Now.AddDays(-19),
                    TeamId=2
                }
            };
            var states = new List<TaskStateModel>
            {
                new TaskStateModel
                {
                    Id=1,
                    Value="creaete"
                },
                new TaskStateModel
                {
                    Id=2,
                    Value="canceled"
                },
                new TaskStateModel
                {
                    Id=3,
                    Value="finished"
                }
            };

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Tasks>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<TaskStateModel>().HasData(states);
        }
    }
}
