﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class User
    {
        public int id;
        public string firstname;
        public string lastname;
        public string email;
        public DateTime birthday;
        public DateTime registeredat;
        public int? teamid;

        public override string ToString()
        {
            return $" First name: {firstname}, Last name: {lastname}, Email: {email}, Birthday: {birthday}," +
                $"Registered at: {registeredat}, Team id: {teamid} ";
        }
    }
}
