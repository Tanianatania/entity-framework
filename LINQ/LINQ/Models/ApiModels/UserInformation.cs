﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models.ApiModels
{
    public class UserInformation
    {
        public User user;
        public Project minProject;
        public int countOfTaskInMinProject;
        public int countOfCanceledOrNotFinishedTasks;
        public Tasks longestTask;
    }
}
