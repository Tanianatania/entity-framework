﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models.ApiModels
{
    public class UserWithTasks
    {
        public User user;
        public List<Tasks> tasks;
    }
}
