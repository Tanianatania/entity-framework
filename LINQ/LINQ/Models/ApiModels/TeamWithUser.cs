﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models.ApiModels
{
    public class TeamWithUser
    {
        public int id;
        public string name;
        public List<User> users;

    }
}
