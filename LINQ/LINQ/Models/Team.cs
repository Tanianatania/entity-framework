﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
   public class Team
    {
        public int id;
        public string name;
        public DateTime createdat;

        public override string ToString()
        {
            return $"Id: {id}, Name: {name}, Create at: {createdat}";
        }
    }
}
