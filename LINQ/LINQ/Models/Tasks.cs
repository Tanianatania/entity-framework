﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class Tasks
    {
        public int id;
        public string name;
        public string description;
        public DateTime createdat;
        public DateTime finishedat;
        public int state;
        public int projectid;
        public int performerid;

        public override string ToString()
        {
            return $"Id: {id}, Name: {name}, Description: {description.Replace("\n"," ")}, Create at: {createdat}, Finished at: {finishedat}" +
                $" State: {state}, Project Id: {projectid}, Perfomer Id: {performerid}\n";
        }
    }
}
